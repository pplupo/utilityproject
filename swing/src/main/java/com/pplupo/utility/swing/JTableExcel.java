package com.pplupo.utility.swing;

import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

public class JTableExcel extends JTable {

	private static final String CELL_SEPARATOR = "\t";
	private static final String LINE_BREAK = "\n";
	private static final String ACTION_COMMAND_PASTE = "Paste";
	private static final String ACTION_COMMAND_COPY = "Copy";
	private static final long serialVersionUID = 1L;

	public JTableExcel() {
		super();
		setupCopyToExcel();
	}

	public JTableExcel(int numRows, int numColumns) {
		super(numRows, numColumns);
		setupCopyToExcel();
	}

	public JTableExcel(Object[][] rowData, Object[] columnNames) {
		super(rowData, columnNames);
		setupCopyToExcel();
	}

	public JTableExcel(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
		super(dm, cm, sm);
		setupCopyToExcel();
	}

	public JTableExcel(TableModel dm, TableColumnModel cm) {
		super(dm, cm);
		setupCopyToExcel();
	}

	public JTableExcel(TableModel dm) {
		super(dm);
		setupCopyToExcel();
	}

	private void setupCopyToExcel() {
		setCellSelectionEnabled(true);
		KeyStroke copy = KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK, false);
		KeyStroke paste = KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK, false);
		ActionListener actionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (ACTION_COMMAND_COPY.equals(e.getActionCommand())) {
					StringBuilder contentToExcel = new StringBuilder();
					int selectedColumnCount = getSelectedColumnCount();
					int selectedRowCount = getSelectedRowCount();
					int[] rowsSelected = getSelectedRows();
					int[] columnsSelected = getSelectedColumns();
					if (!((selectedRowCount - 1 == rowsSelected[rowsSelected.length - 1] - rowsSelected[0]
							&& selectedRowCount == rowsSelected.length)
							&& (selectedColumnCount - 1 == columnsSelected[columnsSelected.length - 1]
									- columnsSelected[0] && selectedColumnCount == columnsSelected.length))) {
						JOptionPane.showMessageDialog(null, "Invalid Copy Selection", "Invalid Copy Selection",
								JOptionPane.ERROR_MESSAGE);
						return;
					}
					for (int i : rowsSelected) {
						for (int j : columnsSelected) {
							contentToExcel.append(getValueAt(rowsSelected[i], columnsSelected[j]));
							if (j < selectedColumnCount - 1) {
								contentToExcel.append(CELL_SEPARATOR);
							}
						}
						contentToExcel.append(LINE_BREAK);
					}
					StringSelection dataCopied = new StringSelection(contentToExcel.toString());
					Toolkit.getDefaultToolkit().getSystemClipboard().setContents(dataCopied, dataCopied);
				}
				if (ACTION_COMMAND_PASTE.equals(e.getActionCommand())) {
//					System.out.println("Trying to Paste");
					int startRow = getSelectedRows()[0];
					int startColumn = getSelectedColumns()[0];
					try {
//						System.out.println("String is:" + trstring);
						String[] rows = ((String) Toolkit.getDefaultToolkit().getSystemClipboard().getContents(this)
								.getTransferData(DataFlavor.stringFlavor)).replaceAll("(?sm)\t\t", "\t \t")
										.replaceAll("(?sm)\t\n", "\t \n").replaceAll("\r\n", "\n")
										.replaceAll("\r", "\n").split(LINE_BREAK);
						for (int i = 0; i < rows.length; i++) {
							String row = rows[i];
							String[] cells = row.split(CELL_SEPARATOR);
							for (int j = 0; j < cells.length; j++) {
								String value = cells[j];
								if (startRow + i < getRowCount() && startColumn + j < getColumnCount()) {
									setValueAt(value, startRow + i, startColumn + j);
//									System.out.println("Putting " + value + "at row=" + startRow + i + "column=" + startCol + j);
								}
							}
						}
					} catch (UnsupportedFlavorException | IOException e1) {
						e1.printStackTrace();
					}
				}

			}
		};
		registerKeyboardAction(actionListener, ACTION_COMMAND_COPY, copy, JComponent.WHEN_FOCUSED);
		registerKeyboardAction(actionListener, ACTION_COMMAND_PASTE, paste, JComponent.WHEN_FOCUSED);
	}

}
