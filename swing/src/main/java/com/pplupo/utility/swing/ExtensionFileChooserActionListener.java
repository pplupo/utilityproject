package com.pplupo.utility.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

public class ExtensionFileChooserActionListener implements ActionListener {
	
	private JTextField textField;
	private String extension;
	private String description;
	private boolean directory;

	public ExtensionFileChooserActionListener(JTextField textField, String extension, String description) {
		this.textField = textField;
		if (extension.startsWith(".")) {
			this.extension = extension;
		} else {
			this.extension = "." + extension;
		}
		this.description = description;
	}
	
	public ExtensionFileChooserActionListener(JTextField textField, boolean directory) {
		this.textField = textField;
		this.directory = directory;
	}

	public void actionPerformed(ActionEvent e) {
		JFileChooser jFileChooser = new JFileChooser();
		jFileChooser.setCurrentDirectory(new File("."));
		if (directory) {
			jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		} else {
			jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			jFileChooser.setFileFilter(new FileFilter() {
				
				@Override
				public String getDescription() {
					return description;
				}
				
				@Override
				public boolean accept(File f) {
					return f.getName().endsWith(extension);
				}
				
			});
		}
		
		jFileChooser.showOpenDialog(null);
		if (jFileChooser.getSelectedFile() == null) {
			return;
		}
		String filePath = jFileChooser.getSelectedFile().getAbsolutePath();
		if (directory || filePath.endsWith(extension)) {
			textField.setText(filePath);
		} else {
			textField.setText(filePath + extension);
		}
	}

}
