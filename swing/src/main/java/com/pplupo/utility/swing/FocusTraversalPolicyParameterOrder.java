package com.pplupo.utility.swing;

import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.util.Arrays;
import java.util.List;

public class FocusTraversalPolicyParameterOrder extends FocusTraversalPolicy {

	private List<Component> componentsList;
	
	public FocusTraversalPolicyParameterOrder(Component... components) {
		componentsList = Arrays.asList(components);
	}

	public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
		return componentsList.get((componentsList.indexOf(aComponent) + 1) % componentsList.size());
	}

	public Component getComponentBefore(Container focusCycleRoot, Component aComponent) {
		return componentsList.get((componentsList.indexOf(aComponent) + componentsList.size() - 1) % componentsList.size());
	}

	public Component getDefaultComponent(Container focusCycleRoot) {
		return componentsList.get(0);
	}

	public Component getFirstComponent(Container focusCycleRoot) {
		return componentsList.get(0);
	}

	public Component getLastComponent(Container focusCycleRoot) {
		return componentsList.get(componentsList.size()-1);
	}

}