package com.pplupo.utility.io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 
 * @author Peter P. Lupo
 *
 */
public final class FileUtils {
	
	public static String fileToString(String filePath) {
		File file = new File(filePath);
		StringBuilder content = new StringBuilder();
		try (BufferedReader bufferedFileReader = new BufferedReader(new FileReader(file))){
			while (bufferedFileReader.ready()) {
				content.append(bufferedFileReader.readLine());
				content.append(System.getProperty("line.separator"));
			}
			if (content.length() != 0) {
				content.deleteCharAt(content.lastIndexOf(System.getProperty("line.separator")));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content.toString();
	}
	
	public static void writeStreamToFile(InputStream is, File file) throws IOException {
		createFile(file);
		try (BufferedInputStream inputStream = new BufferedInputStream(is)) {
			try (OutputStream os = new FileOutputStream(file)) {
				byte[] bytes = new byte[1024];
				int read;
				while ((read = inputStream.read(bytes)) > 0) {
					os.write(bytes, 0, read);
				}
			}
		}
	}
	
	public static void writeStringToFile(String content, File file) throws IOException {
		createFile(file);
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
			writer.write(content);	
		}
	}

	private static void createFile(File file) throws IOException {
		if (!file.getParentFile().exists() && !file.getParentFile().mkdirs()) {
			throw new IOException("Could not create directory path.");
		}
		if (!file.exists() && !file.createNewFile()) {
			throw new IOException("Could not create output file.");
		}
	}
	
	/**
	 * This utility class should not be instantiated.
	 */
	private FileUtils() {}

}
