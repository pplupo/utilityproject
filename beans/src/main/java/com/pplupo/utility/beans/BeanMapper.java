package com.pplupo.utility.beans;

import static java.util.Arrays.stream;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Use this class to copy getters/setters between instances of different classes when the
 * method names are the same.
 * 
 * @author Peter P. Lupo
 * 
  */
public final class BeanMapper {
	
	/**
	 * Represents the accessor and mutator methods' prefixes of a Java bean.
	 */
	private enum PropertyMethodType {
		GETTER("get"), SETTER("set"), IS("is");

		private String prefix;

		private PropertyMethodType(String prefix) {
			this.prefix = prefix;
		}

		public String getPrefix() {
			return prefix;
		}
	}
	
	/**
	 * Copies all properties from <code>T</code> to a new instance of
	 * <code>Class<V></code> as long as there is a property in <code>T</code> with
	 * the same name as in <code>V</code>. The properties are accessed by their
	 * accessor methods in <code>V<code> starting with "<code>is</code>" or
	 * "<code>get</code>" and their primitive values or references are set using
	 * <code>T<code> mutator methods (starting with "<code>set</code>").
	 * 
	 * @param <T>         Type of the instance from which the values will be copied
	 *                    source.
	 * @param <V>         Type of the class destination from which a new instance
	 *                    will be created and the values will be copied to.
	 * @param source      Instance from which the properties' values will be copied.
	 * @param destination Class from which a new instance will be created and the
	 *                    properties' values will be copied to.
	 * @return a new instance of <code>Class<V></code> with the properties' values
	 *         copied source <code>T</code>.
	 */
	public static <T, V> V copyBeanAllProperties(T source, Class<V> destination) {
		return copyBeanAllProperties(source, newInstance(destination));
	}

	/**
	 * Copies specified properties from <code>T</code> to <code>V</code> as long as
	 * there is a property in <code>T</code> with the same name as in <code>V</code>
	 * and this property accessor method is included in <code>methodNames</code>.
	 * The properties are accessed by their accessor methods in
	 * <code>V<code> starting with "<code>is</code>" or "<code>get</code>" and their
	 * primitive values or references are set using
	 * 
	 * @param <T>         Type of the instance from which the values will be copied
	 *                    source.
	 * @param <V>         Type of the instance to which the values will be copied.
	 * @param source      Instance from which the properties' values will be copied.
	 * @param destination Instance to which the properties' values will be copied.
	 * @param methodNames Names of the accessor methods that will return valued to
	 *                    be copied. These methods must start with "<code>is</code>"
	 *                    or "<code>get</code>".
	 * @return the <code>destination</code> with values copied from the source
	 *         instance for the methods in <code>methodNames</code>
	 */
	public static <V, T> V copyBeanAllProperties(T source, V destination) {
		return copyBeanPropertiesFiltered(source, destination, s -> true);
	}

	/**
	 * Copies specified properties from <code>T</code> to a new instance of
	 * <code>Class<V></code> as long as there is a property in <code>T</code> with
	 * the same name as in <code>V</code> except if this property accessor method is
	 * included in <code>methodNames</code>. The properties are accessed by their
	 * accessor methods in <code>V<code> starting with "<code>is</code>" or
	 * "<code>get</code>" and their primitive values or references are set using
	 * <code>T<code> mutator methods (starting with "<code>set</code>").
	 * 
	 * @param <T>         Type of the instance from which the values will be copied
	 *                    source.
	 * @param <V>         Type of the class destination from which a new instance
	 *                    will be created and the values will be copied to.
	 * @param source      Instance from which the properties' values will be copied.
	 * @param destination Class from which a new instance will be created and the
	 *                    properties' values will be copied to.
	 * @param methodNames Names of the accessor methods that will be excluded from
	 *                    the copy. These methods names must start with
	 *                    "<code>is</code>" or "<code>get</code>".
	 * @return a new instance of <code>Class<V></code> with the properties' values
	 *         copied source <code>T</code>.
	 */
	public static <T, V> V copyBeanPropertiesExcept(T source, Class<V> destination, String... methodNames) {
		return copyBeanPropertiesExcept(source, newInstance(destination), methodNames);
	}

	/**
	 * Copies specified properties from <code>T</code> <code>V</code> as long as
	 * there is a property in <code>T</code> with the same name as in <code>V</code>
	 * except if this property accessor method is included in
	 * <code>methodNames</code>. The properties are accessed by their accessor
	 * methods in <code>V<code> starting with "<code>is</code>" or
	 * "<code>get</code>" and their primitive values or references are set using
	 * <code>T<code> mutator methods (starting with "<code>set</code>").
	 * 
	 * @param <T>         Type of the instance from which the values will be copied
	 *                    source.
	 * @param <V>         Type of the instance to which the values will be copied.
	 * @param source      Instance from which the properties' values will be copied.
	 * @param destination Instance to which the properties' values will be copied.
	 * @param methodNames Names of the accessor methods that will be excluded from
	 *                    the copy. These methods names must start with
	 *                    "<code>is</code>" or "<code>get</code>".
	 * @return the <code>destination</code> with values copied from the source
	 *         instance for the methods in <code>methodNames</code>
	 */
	public static <T, V> V copyBeanPropertiesExcept(T source, V destination, String... methodNames) {
		return copyBeanPropertiesFiltered(source, destination, methodName -> stream(methodNames)
				.noneMatch(methodName::equalsIgnoreCase));
	}

	/**
	 * Copies specified properties from <code>T</code> to a new instance of
	 * <code>Class<V></code> as long as there is a property in <code>T</code> with
	 * the same name as in <code>V</code> and this property accessor method is
	 * included in <code>methodNames</code>. The properties are accessed by their
	 * accessor methods in <code>V<code> starting with "<code>is</code>" or
	 * "<code>get</code>" and their primitive values or references are set using
	 * <code>T<code> mutator methods (starting with "<code>set</code>").
	 * 
	 * @param <T>         Type of the instance from which the values will be copied
	 *                    source.
	 * @param <V>         Type of the class destination from which a new instance
	 *                    will be created and the values will be copied to.
	 * @param source      Instance from which the properties' values will be copied.
	 * @param destination Class from which a new instance will be created and the
	 *                    properties' values will be copied to.
	 * @param methodNames Names of the accessor methods that will return valued to
	 *                    be copied. These methods names must start with
	 *                    "<code>is</code>" or "<code>get</code>".
	 * @return a new instance of <code>Class<V></code> with the properties' values
	 *         copied source <code>T</code>.
	 */
	public static <T, V> V copyBeanPropertiesOnly(T source, Class<V> destination, String... methodNames) {
		return copyBeanPropertiesOnly(source, newInstance(destination), methodNames);
	}

	/**
	 * Copies specified properties from <code>T</code> to <code>V</code> as long as
	 * there is a property in <code>T</code> with the same name as in <code>V</code>
	 * and this property accessor method is included in <code>methodNames</code>.
	 * The properties are accessed by their accessor methods in
	 * <code>V<code> starting with "<code>is</code>" or "<code>get</code>" and their
	 * primitive values or references are set using
	 * <code>T<code> mutator methods (starting with "<code>set</code>").
	 * 
	 * @param <T>         Type of the instance from which the values will be copied
	 *                    source.
	 * @param <V>         Type of the instance to which the values will be copied.
	 * @param source      Instance from which the properties' values will be copied.
	 * @param destination Instance to which the properties' values will be copied.
	 * @param methodNames Names of the accessor methods that will return valued to
	 *                    be copied. These methods names must start with
	 *                    "<code>is</code>" or "<code>get</code>".
	 * @return the <code>destination</code> with values copied from the source
	 *         instance for the methods in <code>methodNames</code>
	 */
	public static <T, V> V copyBeanPropertiesOnly(T source, V destination, String... methodNames) {
		return copyBeanPropertiesFiltered(source, destination, methodName -> stream(methodNames)
				.anyMatch(methodName::equalsIgnoreCase));
	}

	/**
	 * This utility class should not be instanced.
	 */
	private BeanMapper() {}

	/**
	 * Copies the properties from <code>source</code> to <code>destination</code>
	 * applying a filter to include or exclude the specified properties by it's
	 * accessor method names.
	 * 
	 * @param <T>         Type of the instance from which the values will be copied
	 *                    source.
	 * @param <V>         Type of the class source which a new instance will be
	 *                    created and the values will be copied to.
	 * @param source      Instance from which the properties' values will be copied.
	 * @param destination Instance to which the properties' values will be copied.
	 * @param filter      the filter to include or exclude specific properties from
	 *                    the copy.
	 * @return the <code>destination</code> with the properties copied, according to
	 *         the filter.
	 */
	@SafeVarargs
	private static final <V, T> V copyBeanPropertiesFiltered(T source, V destination,
			Function<String, Boolean>... filter) {
		Map<String, Method> sourceAccessorMethods = getAllMethods(source.getClass(), PropertyMethodType.GETTER,
				PropertyMethodType.IS);
		Map<String, Method> destinationMutatorMethods = getAllMethods(destination.getClass(),
				PropertyMethodType.SETTER);

		destinationMutatorMethods.entrySet().stream().forEach(e -> {
			String destinationMethodName = e.getKey();
			Method sourceMethod = sourceAccessorMethods.get(destinationMethodName);
			Method destinationMethod = e.getValue();
			if (sourceMethod != null && stream(filter).anyMatch(f -> f.apply(sourceMethod.getName()))) {
				sourceMethod.setAccessible(true);
				destinationMethod.setAccessible(true);
				try {
					copyPropertyFromSourceToDestination(source, destination, sourceMethod, destinationMethod);
				} catch (IllegalArgumentException e1) {
					throw new PropertyCopyException(
							"Property could not be copied. The source's return type might not match the destination's parameter type.",
							e1);
				} catch (IllegalAccessException | InvocationTargetException e1) {
					throw new PropertyCopyException(
							"Property could not be copied. There is probably a security manager access restriction.",
							e1);
				}
			}
		});
		return destination;
	}

	private static <T, V> void copyPropertyFromSourceToDestination(T source, V destination, Method sourceMethod,
			Method destinationMethod) throws IllegalAccessException, InvocationTargetException {
		Object returnFrom = sourceMethod.invoke(source);
		if (isReturnTypeWrapper(sourceMethod) && !isReturnTypeWrapper(destinationMethod)
				&& returnFrom == null) {
			if (sourceMethod.getReturnType() != Boolean.class) {
				destinationMethod.invoke(destination, 0);
			} else {
				destinationMethod.invoke(destination, false);
			}
		} else {
			destinationMethod.invoke(destination, returnFrom);
		}
	}
	
	/**
	 * Gets all methods from the class and super classes as long as they have the
	 * selected prefixes.
	 * 
	 * @param <T>      Type of the class from which the methods must be collected.
	 * @param aClass   Class from which the methods must be collected.
	 * @param prefixes used to filter which methods should be collected.
	 * @return The methods from the class and super classes that have the selected
	 *         prefixes.
	 */
	private static <T> Map<String, Method> getAllMethods(Class<T> aClass, PropertyMethodType... prefixes) {
		Class<? super T> clazz = aClass;
		Map<String, Method> methods = new HashMap<>();
		do {
			stream(clazz.getDeclaredMethods())
					.filter(m -> stream(prefixes).anyMatch(p -> m.getName().startsWith(p.getPrefix()))).forEach(m -> {
						String propertyName = m.getName();
						for (PropertyMethodType propertyMethodType : PropertyMethodType.values()) {
							if (propertyName.startsWith(propertyMethodType.getPrefix())) {
								propertyName = propertyName.replace(propertyMethodType.getPrefix(), "");
								break;
							}
						}
						methods.put(propertyName, m);
					});
			clazz = clazz.getSuperclass();
		} while (clazz != null);
		return methods;
	}

	/**
	 * Identifies if the return type of a method is a wrapper class.
	 * 
	 * @param method Method's return type to be examined.
	 * @return true if the return of the method is of one of the following classes:
	 *         <code>Boolean</code>, <code>Character</code>, <code>Byte</code>,
	 *         <code>Short</code>, <code>Integer</code>, <code>Long</code>,
	 *         <code>Float</code>, <code>Double</code>.
	 */
	private static boolean isReturnTypeWrapper(Method method) {
		return method.getReturnType() == Boolean.class || method.getReturnType() == Character.class
				|| method.getReturnType() == Byte.class || method.getReturnType() == Short.class
				|| method.getReturnType() == Integer.class || method.getReturnType() == Long.class
				|| method.getReturnType() == Float.class || method.getReturnType() == Double.class;
	}


	/**
	 * Creates a new instance of the class using it's no-arguments constructor.
	 * 
	 * @param <V>    The type of class from which a new instance must be created.
	 * @param aClass The class from which a new instance must be created.
	 * @return a new instance of <code>Class<V></code>.
	 */
	private static <V> V newInstance(Class<V> aClass) {
		try {
			Constructor<V> destinationConstructor = aClass.getDeclaredConstructor();
			destinationConstructor.setAccessible(true);
			return destinationConstructor.newInstance();
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			throw new PropertyCopyException("Instance of " + aClass.getName()
					+ " could not be created. It probably lacks a no-arguments constructor.", e);
		}
	}

}