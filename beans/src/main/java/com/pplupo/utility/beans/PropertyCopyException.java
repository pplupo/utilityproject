package com.pplupo.utility.beans;

/**
 * @author Peter P. Lupo
 *
 */
public class PropertyCopyException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PropertyCopyException(String message, Throwable cause) {
		super(message, cause);
	}

}
