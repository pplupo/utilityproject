package com.pplupo.utility.beans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import java.io.Serializable;
import java.rmi.Remote;

import org.junit.Test;

/**
 * 
 * @author Peter P. Lupo
 *
 */
public class BeanMapperTest {
	
	private static SourceBean sourceBean = new SourceBean("init", new Object(), 1, true, null, null);

	@Test
	public void testCopyBeanPropertiesExceptTClassOfVStringArray() {
		DestinationBean destinationBean = BeanMapper.copyBeanPropertiesExcept(sourceBean, DestinationBean.class, "getProp2", "getProp3");
		assertSame(sourceBean.getProp1(), destinationBean.getProp1());
		assertNotSame(sourceBean.getProp2(), destinationBean.getProp2());
		assertNotSame(sourceBean.getProp3(), destinationBean.getProp3());
		assertSame(sourceBean.isProp4(), destinationBean.isProp4());
		assertEquals(false, destinationBean.isProp5());
		assertEquals(0d, destinationBean.getProp6(), 0d);
	}

	@Test
	public void testCopyBeanPropertiesOnlyTClassOfVStringArray() {
		DestinationBean destinationBean = BeanMapper.copyBeanPropertiesOnly(sourceBean, DestinationBean.class, "getProp2", "getProp3");
		assertNotSame(sourceBean.getProp1(), destinationBean.getProp1());
		assertSame(sourceBean.getProp2(), destinationBean.getProp2());
		assertSame(sourceBean.getProp3(), destinationBean.getProp3());
		assertNotSame(sourceBean.isProp4(), destinationBean.isProp4());
		assertNotSame(sourceBean.isProp5(), destinationBean.isProp5());
		assertNotSame(sourceBean.getProp6(), destinationBean.getProp6());
		assertEquals(true, destinationBean.isProp5());
		assertEquals(1d, destinationBean.getProp6(), 0d);
	}

	@Test
	public void testCopyBeanAllPropertiesTClassOfV() {
		DestinationBean destinationBean = BeanMapper.copyBeanAllProperties(sourceBean, DestinationBean.class);
		assertSame(sourceBean.getProp1(), destinationBean.getProp1());
		assertSame(sourceBean.getProp2(), destinationBean.getProp2());
		assertSame(sourceBean.getProp3(), destinationBean.getProp3());
		assertSame(sourceBean.isProp4(), destinationBean.isProp4());
		assertEquals(false, destinationBean.isProp5());
		assertEquals(0d, destinationBean.getProp6(), 0d);
	}

	@SuppressWarnings("serial")
	@Test(expected=PropertyCopyException.class)
	public void testReturnParameterTypeMismatch() {
		BeanMapper.copyBeanAllProperties(new Serializable() {
			@SuppressWarnings("unused")
			public int getValue() {return 1;}
		}, new Remote() {
			@SuppressWarnings("unused")
			public void setValue(String value) {}
		});
	}

	public static class SourceBean extends SuperSourceBean {
		private int prop3;
		private boolean prop4;
		private Boolean prop5;
		private Double prop6;
		private float extraPropertyInSourceBean; 
		
		public SourceBean() {}
		
		public SourceBean(String prop1, Object prop2, int prop3, boolean prop4, Boolean prop5, Double prop6) {
			super(prop1,prop2);
			this.prop3 = prop3;
			this.prop4 = prop4;
			this.prop5 = prop5;
			this.prop6 = prop6;
		}

		public int getProp3() {
			return prop3;
		}
		public void setProp3(int prop3) {
			this.prop3 = prop3;
		}
		public boolean isProp4() {
			return prop4;
		}
		public void setProp4(boolean prop4) {
			this.prop4 = prop4;
		}
		public Boolean isProp5() {
			return prop5;
		}
		public void setProp5(Boolean prop5) {
			this.prop5 = prop5;
		}
		public Double getProp6() {
			return prop6;
		}
		public void setProp6(Double prop6) {
			this.prop6 = prop6;
		}
		public float getExtraPropertyInSourceBean() {
			return extraPropertyInSourceBean;
		}
		public void setExtraPropertyInSourceBean(float extraPropertyInSourceBean) {
			this.extraPropertyInSourceBean = extraPropertyInSourceBean;
		}
		public int shouldNotCauseFailuresSourceBean() {
			return 1;
		}
	}
	
	public static class SuperSourceBean {
		private String prop1;
		private Object prop2;
		
		public SuperSourceBean() {}
		
		public SuperSourceBean(String prop1, Object prop2) {
			this.prop1 = prop1;
			this.prop2 = prop2;
		}
		
		public String getProp1() {
			return prop1;
		}
		public void setProp1(String prop1) {
			this.prop1 = prop1;
		}
		public Object getProp2() {
			return prop2;
		}
		public void setProp2(Object prop2) {
			this.prop2 = prop2;
		}
	}
	
	public static class DestinationBean extends SuperDestinationBean {
		private String prop1;
		private Object prop2;
		private boolean prop5 = true;
		private double prop6 = 1d;
		private int extraMethodinDestinationBean;

		public DestinationBean() {}
		
		public String getProp1() {
			return prop1;
		}
		public void setProp1(String prop1) {
			this.prop1 = prop1;
		}
		public Object getProp2() {
			return prop2;
		}
		public void setProp2(Object prop2) {
			this.prop2 = prop2;
		}
		public boolean isProp5() {
			return prop5;
		}
		public void setProp5(boolean prop5) {
			this.prop5 = prop5;
		}
		public double getProp6() {
			return prop6;
		}
		public void setProp6(double prop6) {
			this.prop6 = prop6;
		}
		public int getExtraMethodinDestinationBean() {
			return extraMethodinDestinationBean;
		}
		public void setExtraMethodinDestinationBean(int extraMethod) {
			this.extraMethodinDestinationBean = extraMethod;
		}
		public String shouldNotCauseFailuresDestinationBean() {
			return "";
		}
	}
	
	
	public static class SuperDestinationBean {
		private int prop3;
		private boolean prop4;
		
		public int getProp3() {
			return prop3;
		}
		public void setProp3(int prop3) {
			this.prop3 = prop3;
		}
		public boolean isProp4() {
			return prop4;
		}
		public void setProp4(boolean prop4) {
			this.prop4 = prop4;
		}
	}

}
