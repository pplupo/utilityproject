package com.pplupo.utility.jsf;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@SessionScoped
public class LocaleSelection implements Serializable {

	private static final long serialVersionUID = 1L;

	private Locale locale;

	private static Map<String,Object> supportedLocales;
	static{
		supportedLocales = new LinkedHashMap<String,Object>();
		supportedLocales.put("English", new Locale("en"));
		supportedLocales.put("PortuguÍs", new Locale("pt"));
	}

	public Map<String, Object> getSupportedLocales() {
		return supportedLocales;
	}

    @PostConstruct
    public void init() {
        locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
    }

    public Locale getLocale() {
        return locale;
    }

    public String getLanguage() {
        return getLocale().getLanguage();
    }

    public void setLanguage(String language) {
        locale = new Locale(language);
        FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
    }

}