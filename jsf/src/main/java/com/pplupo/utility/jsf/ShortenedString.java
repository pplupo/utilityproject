package com.pplupo.utility.jsf;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.pplupo.utility.string.StringUtils;

@SuppressWarnings("serial")
@FacesConverter(value="shortenedString")
@SessionScoped
public class ShortenedString implements Converter, Serializable {
	
	private static int maxLength = 200; //set up maximum length.
	
	@Override
	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String string) {
		return string;
	}

	@Override
	public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
		if (object == null) {
			return "";
		}
		return StringUtils.shortenStringWithEllipsis(object.toString(), maxLength);
	}

}
