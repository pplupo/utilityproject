package com.pplupo.utility.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import com.pplupo.utility.string.StringUtils;


@SuppressWarnings("serial")
public abstract class AbstractBaseManagedBean implements Serializable {
	
	private static final String BUNDLE_PREFIX = "com.pplupo.utility.messages"; //REQUIRE CONFIGURATION
	
	protected void facesContextMessage(Object message) {
		facesContextMessage(new FacesMessage(message.toString()));
	}
	
	protected void facesContextMessage(FacesMessage message) {
		FacesContext.getCurrentInstance().addMessage(null, message);
	}
	
	protected String getMessage(String key) {
		return ResourceBundle.getBundle(BUNDLE_PREFIX, FacesContext.getCurrentInstance().getViewRoot().getLocale(),
					Thread.currentThread().getContextClassLoader()).getString(key);
	}
	
	protected String getFormattedMessage(String key, Object... args) {
		return MessageFormat.format(getMessage(key), args);
	}

	protected void validateSameValue(String value1, String value2, String messageKey) {
		if (!StringUtils.handleNull(value1).equals(value2)) {
			validationException(messageKey);
		}
	}

	protected void validationException(String messageKey) {
		throw new ValidatorException(validationErrorMessage(messageKey));
	}

	protected FacesMessage validationErrorMessage(String messageKey) {
		return new FacesMessage(FacesMessage.SEVERITY_ERROR, getMessage(messageKey), getMessage(messageKey));
	}

	protected void redirect(String destination) throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect(destination);
	}
}
