package com.pplupo.utility.binary;

public class BitFlags {
	
	private static final long ALL_ON_64BITS = 0xFFFFFFFFFFFFFFFFl;
	private static final int ALL_ON_32BITS = 0xFFFFFFF;
	private static final long ZERO_LONG = 0l;
	private static final int ZERO = 0;

	private BitFlags() {}
	
	public static int bitFlag(int... bitPositionsToSet) {
		int result = 0;
		for (int bitPosition : bitPositionsToSet) {
			result += Math.pow(2, bitPosition);
		}
		return result;
	}
	
	/**
	 * Sets the specified flags on the source int
	 *
	 * @param source the source int
	 * @param flag   the flags which should be set
	 *
	 * @return the set int
	 */
	public static int setFlag(int source, int flag) {
	    return source | flag;
	}

	/**
	 * Un-sets the specified flags on the source int
	 *
	 * @param source the source int
	 * @param flag   the flags which should be set
	 *
	 * @return the set int
	 */
	public static int unsetFlag(int source, int flag) {
	    return source & ~flag;
	}


	/**
	 * Check if the flags are set on the source ints
	 *
	 * @param source the source int
	 * @param flag   the flags which should be set
	 *
	 * @return the set int
	 */
	public static boolean isSet(int source, int flag) {
	    return (source & flag) == flag;
	}


	/**
	 * Flips the specified bit on the source
	 *
	 * @param source the source int
	 * @param flag   the flags which should be set
	 *
	 * @return the set int
	 */
	public static int flip(int source, int flag) {
		for (int i = 1; i < flag; ++i) {
			source |= source << 1;
		}
		return source;
	}
	
    /**
     * Turn off all flags.
     */
    public static int turnOffAllInt() {
        return ZERO;
    }
    
    /**
     * Turn off all flags.
     */
    public static long turnOffAllLong() {
        return ZERO_LONG;
    }

    
    /**
     * Turn on all 32 flags.
     */
    public static int turnOnAllInt() {
        return ALL_ON_32BITS;
    }
    
    /**
     * Turn on all 32 flags.
     */
    public static long turnOnAllLong() {
        return ALL_ON_64BITS;
    }

	/**
	 * Returns the masked int
	 *
	 * @param source the source int
	 * @param mask
	 *
	 * @return the set int
	 */
	public static int mask(int source, int mask) {
	    return source & mask;
	}
	
    public String asString(int source) {
        StringBuilder bin = new StringBuilder(Long.toBinaryString(source));
        for (int i = 64 - bin.length(); i > 0; i--) {
            bin.insert(0, "0");
        }
        return bin.toString();
    }


}
