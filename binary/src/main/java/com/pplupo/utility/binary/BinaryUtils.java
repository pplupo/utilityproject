package com.pplupo.utility.binary;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Calendar;

/**
 * 
 * @author Peter P. Lupo
 *
 */
public final class BinaryUtils {
	
    public static String byteArrayToHex(byte[] bytes, int start, int end) {
		StringBuilder sb = new StringBuilder();
	    for (int i = start; i < end; i++) {
	        sb.append(String.format("%02X", bytes[i]));
	    }
	    return sb.toString().toLowerCase();
	}
    
    public static int bytesToInt(byte[] bytes) {
		byte[] minLength = new byte[4];
		System.arraycopy(bytes, 0, minLength, 0, bytes.length);
		return ByteBuffer.wrap(minLength).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
	}

	public static byte[] convertDate(Calendar date) {
		byte[] data = intToBytes(date.get(Calendar.YEAR));
		data[2] = intToBytes(date.get(Calendar.MONTH)+1)[0];
		data[3] = intToBytes(date.get(Calendar.DAY_OF_MONTH))[0];
		return data;
	}
	
	public static byte[] copyOfRange(byte[] original, int from, int to) {
        int newLength = to - from;
        if (newLength < 0) {
        	throw new IllegalArgumentException("The copy would start before the end:" + from + " > " + to);
        }
        byte[] copy = new byte[newLength];
        System.arraycopy(original, from, copy, 0, Math.min(original.length - from, newLength));
        return copy;
    }
	
	public static byte[] hexToByteArray(String hexBytes) {
	    int len = hexBytes.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(hexBytes.charAt(i), 16) << 4) + Character.digit(hexBytes.charAt(i+1), 16));
	    }
	    return data;
	}
	
	public static byte[] intToBytes(int value) {
		ByteBuffer b = ByteBuffer.allocate(4);
		b.order(ByteOrder.LITTLE_ENDIAN);
		b.putInt(value);
		return b.array();
	}
	
	/**
	 * This utility class should not be instantiated.
	 */
	private BinaryUtils() {}

}
