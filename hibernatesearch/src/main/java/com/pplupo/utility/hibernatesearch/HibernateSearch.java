package com.pplupo.utility.hibernatesearch;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;

public abstract class HibernateSearch {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public <T> List<T> search(String query, Class<T> clazz) {
		FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

		// create native Lucene query unsing the query DSL alternatively you can
		// write the Lucene query using the Lucene query parser or the Lucene
		// programmatic API. The Hibernate Search DSL is recommended though
		QueryBuilder qb = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(clazz).get();

		org.apache.lucene.search.Query luceneQuery = qb.keyword()
				.onFields(getFieldNames())
				.matching(query).createQuery();

		// wrap Lucene query in a javax.persistence.Query
		javax.persistence.Query jpaQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, clazz);

		// execute search
		return jpaQuery.getResultList();
	}
	
	//Override this method to set search field names
	protected String[] getFieldNames() {
		return new String[]{"field.name1", "field.name2", "field.name3", "field.name4"};
	}

}
