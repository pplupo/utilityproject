package com.pplupo.utility.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet(name = "File Download", urlPatterns = { "/downloadurl/*" }) //change the download URL
public class FileDownload extends HttpServlet {


	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		byte[] data = null;// Get data to send.
		res.setStatus(HttpServletResponse.SC_OK);
		res.setContentType("provide filetype"); //"application/pdf", "image/jpeg", etc...
		try (ServletOutputStream out = res.getOutputStream()) {
			out.write(data);
			out.flush();
		}
	}

}