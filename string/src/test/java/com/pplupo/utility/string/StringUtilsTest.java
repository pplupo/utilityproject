package com.pplupo.utility.string;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author Peter P. Lupo
 * @since Oct 3, 2004
 */
public class StringUtilsTest {

	@Test
	public void testPadLeft() {
		assertEquals("PadLeftTest1        ", StringUtils.padLeft("PadLeftTest1", 20));
		assertEquals("PadLeft", StringUtils.padLeft("PadLeftTest1", 7));
		assertEquals("PadLeftTest1", StringUtils.padLeft("PadLeftTest1", 12));
	}

	@Test
	public void testPadRight() {
		assertEquals("       PadRightTest1", StringUtils.padRight("PadRightTest1", 20));
		assertEquals("PadRight", StringUtils.padRight("PadRightTest1", 8));
		assertEquals("PadRightTest1", StringUtils.padRight("PadRightTest1", 13));
	}

}
