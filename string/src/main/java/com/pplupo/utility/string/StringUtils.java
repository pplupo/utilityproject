package com.pplupo.utility.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Peter P. Lupo
 */
public final class StringUtils {
	
	private static final String HAS = "has";
	private static final String IS = "is";
	private static final String SET = "set";
	private static final String GET = "get";
	private static final String SPLIT_CAMEL_CASE_FORMAT = "%s|%s|%s";
	private static final String SPLIT_CAMEL_CASE_ARG0  = "(?<=[A-Z])(?=[A-Z][a-z])";
	private static final String SPLIT_CAMEL_CASE_ARG1  = "(?<=[^A-Z])(?=[A-Z])";
	private static final String SPLIT_CAMEL_CASE_ARG2  = "(?<=[A-Za-z])(?=[^A-Za-z])";
	private static final String CAMEL_CASE_TOKEN = "(^|_)(.)";
	private static final char EMPTY_SPACE_CHARACTER = ' ';
	private static final String EMTPY_SPACE = " ";
	private static final String ELLIPSIS = "...";
	private static final char UNDERSCORE_CHARACTER = '_';
	

	public static String handleNull(String string) {
		if (isNullOrEmpty(string)) {
			return "";
		}
		return string;
	}
	
	public static boolean isNullOrEmpty(String string) {
		return "".equals(string) || null == string;
	}

	public static String buildGetterName(String humanReadable) {
		return GET + camelCase(humanReadable, true, EMPTY_SPACE_CHARACTER);
	}

	public static String buildSetterName(String humanReadable) {
		return SET + camelCase(humanReadable, true, EMPTY_SPACE_CHARACTER);
	}
	
	public static String buildIsName(String humanReadable) {
		return IS + camelCase(humanReadable, true, EMPTY_SPACE_CHARACTER);
	}
	
	public static String buildHasName(String humanReadable) {
		return HAS + camelCase(humanReadable, true, EMPTY_SPACE_CHARACTER);
	}
	
	public static String shortenStringWithEllipsis(String string, int maxLength) {
		String value = handleNull(string);
		if (value.length() > maxLength) {
			return value.substring(0, maxLength - 4) + ELLIPSIS;
		}
		return value;
	}

	/**
	 * By default, this method converts strings to UpperCamelCase. If the
	 * <code>uppercaseFirstLetter</code> argument to false, then this method
	 * produces lowerCamelCase. This method will also use any extra delimiter
	 * characters to identify word boundaries.
	 * <p>
	 * Examples:
	 * 
	 * <pre>
	 *   camelCase(&quot;active_record&quot;,false)    #=&gt; &quot;activeRecord&quot;
	 *   camelCase(&quot;active_record&quot;,true)     #=&gt; &quot;ActiveRecord&quot;
	 *   camelCase(&quot;first_name&quot;,false)       #=&gt; &quot;firstName&quot;
	 *   camelCase(&quot;first_name&quot;,true)        #=&gt; &quot;FirstName&quot;
	 *   camelCase(&quot;name&quot;,false)             #=&gt; &quot;name&quot;
	 *   camelCase(&quot;name&quot;,true)              #=&gt; &quot;Name&quot;
	 * </pre>
	 * 
	 * </p>
	 * 
	 * @param lowerCaseAndUnderscoredWord the word that is to be converted to camel
	 *                                    case
	 * @param uppercaseFirstLetter        true if the first character is to be
	 *                                    uppercased, or false if the first
	 *                                    character is to be lowercased
	 * @param delimiterChars              optional characters that are used to
	 *                                    delimit word boundaries
	 * @return the camel case version of the word
	 * @see #underscore(String, char[])
	 * @see #upperCamelCase(String, char[])
	 * @see #lowerCamelCase(String, char[])
	 */
	public static String camelCase(String lowerCaseAndUnderscoredWord, boolean uppercaseFirstLetter,
			char... delimiterChars) {
		if (lowerCaseAndUnderscoredWord == null)
			return null;
		lowerCaseAndUnderscoredWord = lowerCaseAndUnderscoredWord.trim();
		if (lowerCaseAndUnderscoredWord.length() == 0)
			return "";
		if (uppercaseFirstLetter) {
			String result = lowerCaseAndUnderscoredWord;
			// Replace any extra delimiters with underscores (before the
			// underscores are converted in the next step)...
			if (delimiterChars != null) {
				for (char delimiterChar : delimiterChars) {
					result = result.replace(delimiterChar, UNDERSCORE_CHARACTER);
				}
			}
			// Change the case at the beginning at after each underscore ...
			return replaceAllWithUppercase(result, CAMEL_CASE_TOKEN, 2);
		}
		if (lowerCaseAndUnderscoredWord.length() < 2)
			return lowerCaseAndUnderscoredWord;
		return Character.toLowerCase(lowerCaseAndUnderscoredWord.charAt(0))
				+ camelCase(lowerCaseAndUnderscoredWord, true, delimiterChars).substring(1);
	}


	/**
	 * Pads the string with empty spaces to the left until the string reaches the total length.
	 * @param toPad String to be padded.
	 * @param totalLength the total length of the returned string.
	 * @return String padded with empty spaces on the left.
	 */
	public static String padLeft(String toPad, int totalLength) {
		if (toPad.length() >= totalLength)
			return toPad.substring(0, totalLength);
		return toPad + emptySpaces(totalLength - toPad.length());
	}

	/**
	 * Pads the string with empty spaces to the right until the string reaches the total length.
	 * @param toPad String to be padded.
	 * @param totalLength the total length of the returned string.
	 * @return String padded with empty spaces on the right.
	 */
	public static String padRight(String toPad, int totalLength) {
		if (toPad.length() >= totalLength)
			return toPad.substring(0, totalLength);
		return emptySpaces(totalLength - toPad.length()) + toPad;
	}

	/**
	 * Utility method to replace all occurrences given by the specific backreference
	 * with its uppercased form, and remove all other backreferences.
	 * <p>
	 * The Java {@link Pattern regular expression processing} does not use the
	 * preprocessing directives <code>\l</code>, <code>&#92;u</code>,
	 * <code>\L</code>, and <code>\U</code>. If so, such directives could be used in
	 * the replacement string to uppercase or lowercase the backreferences. For
	 * example, <code>\L1</code> would lowercase the first backreference, and
	 * <code>&#92;u3</code> would uppercase the 3rd backreference.
	 * </p>
	 * 
	 * @param input
	 * @param regex
	 * @param groupNumberToUppercase
	 * @return the input string with the appropriate characters converted to
	 *         upper-case
	 */
	public static String replaceAllWithUppercase(String input, String regex, int groupNumberToUppercase) {
		Pattern underscoreAndDotPattern = Pattern.compile(regex);
		Matcher matcher = underscoreAndDotPattern.matcher(input);
		// CHECKSTYLE IGNORE check FOR NEXT 1 LINES
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, matcher.group(groupNumberToUppercase).toUpperCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	public static String splitCamelCase(String s) {
		return s.replaceAll(String.format(SPLIT_CAMEL_CASE_FORMAT, SPLIT_CAMEL_CASE_ARG0,
				SPLIT_CAMEL_CASE_ARG1, SPLIT_CAMEL_CASE_ARG2), EMTPY_SPACE);
	}

	/**
	 * This utility class should not be instantiated.
	 */
	private StringUtils() {
	}


	/**
	 * Creates a string with space characters.
	 * @param length the string length.
	 * @return A string of empty spaces.
	 */
	private static String emptySpaces(int length) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++)
			sb.append(EMTPY_SPACE);
		return sb.toString();
	}

}
