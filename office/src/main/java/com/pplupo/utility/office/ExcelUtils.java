package com.pplupo.utility.office;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {


	public static void readExcel(File file) throws IOException {
		Workbook workbook = new XSSFWorkbook(new FileInputStream(file));
		Sheet sheet1 = workbook.getSheet("Acessos");
		for (Row row : sheet1) {
			for (Cell cell : row) {
				System.out.println(readCellData(cell));
			}
		}
		workbook.close();
	}

	private static String readCellData(Cell cell) {
		switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING:
				return cell.getStringCellValue().trim();
			case Cell.CELL_TYPE_NUMERIC:
				return String.valueOf(cell.getNumericCellValue()).trim();
			case Cell.CELL_TYPE_BOOLEAN:
				return String.valueOf(cell.getBooleanCellValue()).trim();
		}
		return "";
	}

	public static void importFile(InputStream data) throws IOException, InvalidFormatException {
		Workbook workbook = WorkbookFactory.create(data);
		Sheet sheet = workbook.getSheetAt(0);
//		Sheet sheet = workbook.getSheet("Name");
		for (Row row : sheet) {
			if (row.getCell(0).getCellType() == Cell.CELL_TYPE_ERROR) {
				continue;
			}
			for (Cell cell : row) {
				System.out.println(readCellData(cell));
			}
		}
		workbook.close();
	}
	
	private ExcelUtils() {}
}