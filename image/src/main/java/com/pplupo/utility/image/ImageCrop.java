package com.pplupo.utility.image;

import java.awt.Dimension;
import java.awt.Transparency;
import java.awt.image.BufferedImage;

public class ImageCrop {

	private static final int BLUE = 0x000000FF;
	private static final int GREEN = 0x0000FF00;
	private static final int RED = 0x00FF0000;
	private static final int ALPHA = 0xFF000000;

	public enum Format {
		PNG, JPG, GIF, BMP, UNKNOWN;
	}

	private ImageCrop() {
	}

	public static BufferedImage getCroppedImage(BufferedImage source, double tolerance, Format format) {
		return getCroppedImage(source, tolerance, format, new Dimension(source.getWidth(), source.getHeight()));
	}

	public static BufferedImage getCroppedImage(BufferedImage source, double tolerance, Format format,
			Dimension scaledDimension) {
		// Get our top-left pixel color as our "baseline" for cropping
		final int baseColor = source.getRGB(0, 0);

		int topY = Integer.MAX_VALUE;
		int topX = Integer.MAX_VALUE;
		int bottomY = -1;
		int bottomX = -1;
		for (int y = 0; y < source.getHeight(); y++) {
			for (int x = 0; x < source.getWidth(); x++) {
				if (colorWithinTolerance(baseColor, source.getRGB(x, y), tolerance)) {
					topX = Math.min(x, topX);
					topY = Math.min(y, topY);
					bottomX = Math.max(x, bottomX);
					bottomY = Math.max(y, bottomY);
				}
			}
		}

		final int unscaledWidth = bottomX - topX + 1;
		final int unscaledHeight = bottomY - topY + 1;

		final double ratio = Math.min(scaledDimension.getWidth() / unscaledWidth,
				scaledDimension.getHeight() / unscaledHeight);

		final int scaledWidth = (int) ((bottomX - topX + 1) * ratio);
		final int scaledHeight = (int) ((bottomY - topY + 1) * ratio);

		final int imageType = imageTypeSelector(source.getTransparency(), format);

		final BufferedImage destination = new BufferedImage(scaledWidth, scaledHeight, imageType);

		destination.getGraphics().drawImage(source, 0, 0, scaledWidth, scaledHeight, topX, topY, bottomX, bottomY,
				null);

		return destination;
	}

	private static int imageTypeSelector(int transparency, Format format) {
		switch (format) {
			case PNG: {
				return BufferedImage.TYPE_INT_ARGB;
			}
			case JPG: {
				return BufferedImage.TYPE_INT_RGB;
			}
			case GIF: {
				return BufferedImage.TYPE_4BYTE_ABGR;
			}
			case BMP: {
				return BufferedImage.TYPE_INT_RGB;
			}
			case UNKNOWN: {
				if (Transparency.TRANSLUCENT == transparency) {
					return BufferedImage.TYPE_INT_ARGB;
				} else {
					return BufferedImage.TYPE_INT_RGB;
				}
			}
			default: {
				throw new IllegalArgumentException("No image format selected.");
			}
		}
	}

	private static boolean colorWithinTolerance(int a, int b, double tolerance) {
		final int aAlpha = (a & ALPHA) >>> 24;
		final int aRed = (a & RED) >>> 16;
		final int aGreen = (a & GREEN) >>> 8;
		final int aBlue = a & BLUE;

		final int bAlpha = (b & ALPHA) >>> 24;
		final int bRed = (b & RED) >>> 16;
		final int bGreen = (b & GREEN) >>> 8;
		final int bBlue = b & BLUE;

		final double distance = Math.sqrt((aAlpha - bAlpha) * (aAlpha - bAlpha) + (aRed - bRed) * (aRed - bRed)
				+ (aGreen - bGreen) * (aGreen - bGreen) + (aBlue - bBlue) * (aBlue - bBlue));

		// 510.0 is the maximum distance between two colors
		// (0,0,0,0 -> 255,255,255,255)
		return ((distance / 510.0d) > tolerance);
	}

}