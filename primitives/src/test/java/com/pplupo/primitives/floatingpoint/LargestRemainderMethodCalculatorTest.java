package com.pplupo.primitives.floatingpoint;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;

import org.junit.Test;

public class LargestRemainderMethodCalculatorTest {

	@Test
	public void testCalculateDoubleDoubleArray() {
		double[] doublePartials = {3.32d, 3.35d, 3.31d};
		double doubleTotal = 10d;
		double[] doubleResult = LargestRemainderMethodCalculator.calculate(doubleTotal, doublePartials);
		assertEquals(3.3266533066132262, doubleResult[0], 0);
		assertEquals(3.3567134268537075, doubleResult[1], 0);
		assertEquals(3.3166332665330662, doubleResult[2], 0);
		assertNotEquals(10, (int)Arrays.stream(doublePartials).reduce(0, Double::sum));
		assertEquals(10, (int)Arrays.stream(doubleResult).reduce(0, Double::sum));
	}
	
	@Test
	public void testCalculateBigDecimalBigDecimalArray() {
		BigDecimal[] bigDecimalPartials = {BigDecimal.valueOf(3.32d), BigDecimal.valueOf(3.35d), BigDecimal.valueOf(3.31d)};
		BigDecimal bigDecimalTotal = BigDecimal.valueOf(10d);
		//Checks test precondition, not the method execution.
		assertNotEquals(Arrays.stream(bigDecimalPartials).reduce(BigDecimal.ZERO, (a, b) -> a.add(b, MathContext.DECIMAL64)), bigDecimalTotal);
		
		BigDecimal[] bigDecimalResult = LargestRemainderMethodCalculator.calculate(bigDecimalTotal, bigDecimalPartials);
		assertEquals(BigDecimal.valueOf(3.3266533066132262), bigDecimalResult[0]);
		assertEquals(BigDecimal.valueOf(3.3567134268537075), bigDecimalResult[1]);
		assertEquals(BigDecimal.valueOf(3.3166332665330662), bigDecimalResult[2]);
		assertEquals(0, Arrays.stream(bigDecimalResult).reduce(BigDecimal.ZERO, (a, b) -> a.add(b, MathContext.DECIMAL64)).compareTo(bigDecimalTotal));
	}

}
