package com.pplupo.primitives.converters;

import static com.pplupo.primitives.converters.BooleanConverter.convert;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.Test;

public class BooleanConverterTest {

	@Test
	public void testConvertString() {
		assertFalse(convert("false"));
		assertFalse(convert("False"));
		assertFalse(convert("falSe"));
		assertFalse(convert("FALSE"));
		assertFalse(convert("off"));
		assertFalse(convert("Off"));
		assertFalse(convert("ofF"));
		assertFalse(convert("no"));
		assertFalse(convert("No"));
		assertFalse(convert("nO"));
		assertFalse(convert("n"));
		assertFalse(convert("N"));
		
		assertFalse(convert("f"));
		assertFalse(convert("F"));
		
		assertFalse(convert(""));
		
		assertFalse(convert("0"));
		
		String value = null;
		assertFalse(convert(value));
		
		assertTrue(convert("a"));
	}
	
	@Test
	public void testConvertInt() {
		
		assertFalse(convert(0));
		assertTrue(convert(1));
		assertTrue(convert(-1));
		
	}
	
	@Test
	public void testConvertLong() {
		assertFalse(convert(0L));
		assertTrue(convert(1L));
		assertTrue(convert(-1L));
	}
	
	@Test
	public void testConvertInteger() {
		assertFalse(convert(Integer.valueOf(0)));
				
		Integer value = null;
		assertFalse(convert(value));
		
		assertTrue(convert(Integer.valueOf(1)));
		assertTrue(convert(Integer.valueOf(-1)));
	}
	
	@Test
	public void testConvertLongWrapper() {
		assertFalse(convert(Long.valueOf(0L)));
		
		Long value = null;
		assertFalse(convert(value));
		
		assertTrue(convert(Long.valueOf(1L)));
		assertTrue(convert(Long.valueOf(-1L)));
		
	}
	
	@Test
	public void testConvertBigInteger() {
		assertFalse(convert(BigInteger.ZERO));
		
		BigInteger value = null;
		assertFalse(convert(value));
		
		assertTrue(convert(BigInteger.valueOf(1L)));
		assertTrue(convert(BigInteger.valueOf(-1L)));
	}
	
}
