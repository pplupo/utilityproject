package com.pplupo.primitives.floatingpoint;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;

public class LargestRemainderMethodCalculator {
	
	private LargestRemainderMethodCalculator() {}

	public static double[] calculate(double total, double... partials) {
		BigDecimal[] result = calculate(BigDecimal.valueOf(total),
				Arrays.stream(partials).mapToObj(BigDecimal::valueOf).toArray(size -> new BigDecimal[size]));
		return Arrays.stream(result).mapToDouble(BigDecimal::doubleValue).toArray();
	}
	
	public static BigDecimal[] calculate(BigDecimal total, BigDecimal... partials) {
		if (partials == null || partials.length == 0) {
			throw new IllegalArgumentException("There are no partials to add.");
		}
		BigDecimal[] newPartials = Arrays.copyOf(partials, partials.length);
		BigDecimal[] reminders = new BigDecimal[newPartials.length];

		BigDecimal totalPartials = BigDecimal.ZERO;
		for (BigDecimal partial : newPartials) {
			totalPartials = totalPartials.add(partial);
		}
		if (totalPartials.compareTo(BigDecimal.ZERO) == 0) {
			return new BigDecimal[] {BigDecimal.ZERO};
		}

		BigDecimal quota = BigDecimal.valueOf(totalPartials.doubleValue() / total.doubleValue());
		BigDecimal allocatedPartials = BigDecimal.ZERO;

		for (int i = 0; i < newPartials.length; i++) {
			BigDecimal partialDivQuota = BigDecimal.valueOf(newPartials[i].doubleValue() / quota.doubleValue());
			newPartials[i] = partialDivQuota;
			reminders[i] = partialDivQuota.subtract(newPartials[i]);
			allocatedPartials = allocatedPartials.add(newPartials[i], MathContext.DECIMAL64);
		}

		BigDecimal partialsToBeRedistributed = total.subtract(allocatedPartials);
		
		for (int i = 0; partialsToBeRedistributed.compareTo(new BigDecimal(i)) > 0; i++) {
			BigDecimal max = BigDecimal.ZERO;
			BigDecimal maxIndex = BigDecimal.ZERO;
			for (int j = 0; j < reminders.length; j++) {
				if (reminders[j].compareTo(max) > 0) {
					max = reminders[j];
					maxIndex = BigDecimal.valueOf(j);
				}
			}
			newPartials[maxIndex.intValue()] = newPartials[maxIndex.intValue()].add(BigDecimal.ONE);
			reminders[maxIndex.intValue()] = BigDecimal.ZERO;
		}

		return newPartials;
	}

}
