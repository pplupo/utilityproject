package com.pplupo.primitives.converters;

import java.math.BigInteger;

public class BooleanConverter {
	
	private static final String FALSE = "false";
	private static final String F = "f";
	private static final String NO = "no";
	private static final String N = "n";
	private static final String OFF = "off";
	private static final String EMPTY = "";
	private static final String ZERO = "0";
	
	private BooleanConverter() {}
	
	/**
	 * Convert a string to boolean.
	 * The following values will be considered false:
	 * <ul>
	 * <li>"false"</li>
	 * <li>"f"</li>
	 * <li>"no"</li>
	 * <li>"n"</li>
	 * <li>"off"</li>
	 * <li>"0"</li>
	 * <li>"" - <i>empty string</i></li>
	 * <li>null</li>
	 * </ul>
	 * The values are case insensitive. Leading and trailing spaces are disregarded.
	 * Any other value will be considered true.
	 * @param value the string value to be converted to boolean.
	 * @return false if it matches one of the string equivalents to false. True otherwise.
	 */
	public static boolean convert(String value) {
		if (null == value) {
			return false;
		}
		final String trimmedValue = value.trim();
		return !(FALSE.equalsIgnoreCase(trimmedValue)
				|| F.equalsIgnoreCase(trimmedValue)
				|| NO.equalsIgnoreCase(trimmedValue)
				|| N.equalsIgnoreCase(trimmedValue)
				|| OFF.equalsIgnoreCase(trimmedValue)
				|| EMPTY.equalsIgnoreCase(trimmedValue)
				|| ZERO.equals(trimmedValue));
		
	}
	
	/**
	 * Convert the value 0 to boolean false. Any other value is converted to boolean true.
	 * @param value the value to be converted to boolean.
	 * @return false if value is 0. True otherwise.
	 */
	public static boolean convert(int value) {
		return convert((long) value);
	}

	/**
	 * Convert the value 0 to boolean false. Any other value is converted to boolean true.
	 * @param value the value to be converted to boolean.
	 * @return false if value is 0. True otherwise.
	 */
	public static boolean convert(long value) {
		return 0 != value;
	}
	
	/**
	 * Convert the value 0 to boolean false. Any other value is converted to boolean true.
	 * @param value the value to be converted to boolean.
	 * @return false if value is 0. True otherwise.
	 */
	public static boolean convert(Integer value) {
		if (value == null) {
			return false;
		}
		return convert(value.longValue());
	}
	
	/**
	 * Convert the value 0 to boolean false. Any other value is converted to boolean true.
	 * @param value the value to be converted to boolean.
	 * @return false if value is 0. True otherwise.
	 */
	public static boolean convert(Long value) {
		if (value == null) {
			return false;
		}
		return convert(value.longValue());
	}
	
	/**
	 * Convert the value 0 to boolean false. Any other value is converted to boolean true.
	 * @param value the value to be converted to boolean.
	 * @return false if value is 0. True otherwise.
	 */
	public static boolean convert(BigInteger value) {
		if (value == null) {
			return false;
		}
		return convert(value.longValue());
	}
	
}